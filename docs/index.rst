================
SLEPc for Python
================

:Author:       Lisandro Dalcin
:Contact:      dalcinl@gmail.com
:Organization: `CIMEC <http://www.cimec.org.ar/>`_
:Address:      CCT CONICET, 3000 Santa Fe, Argentina


Online Documentation
--------------------

+ `User Manual (HTML)`_ (generated with Sphinx_).
+ `User Manual (PDF)`_  (generated with Sphinx_).
+ `API Reference`_      (generated with Epydoc_).

.. _User Manual (HTML): usrman/index.html
.. _User Manual (PDF):  slepc4py.pdf
.. _API Reference:      apiref/index.html

.. _Sphinx:    http://sphinx.pocoo.org/
.. _Epydoc:    http://epydoc.sourceforge.net/


Discussion and Support
----------------------

+ Mailing Lists: petsc-users@mcs.anl.gov, slepc-maint@upv.es


Downloads and Development
-------------------------

+ Source Releases: https://bitbucket.org/slepc/slepc4py/downloads/
+ Issue Tracker:   https://bitbucket.org/slepc/slepc4py/issues/
+ Repository:      https://bitbucket.org/slepc/slepc4py.git


Acknowledgments
---------------

This project was partially supported by the Center for Numerical
Porous Media, Division of Computer, Electrical, and Mathematical
Sciences & Engineering, King Abdullah University of Science and
Technology (KAUST).
